provider "aws" {
  region  = var.region
  access_key = "access_key_ID"
  secret_key = "secret_key_ID"
}

